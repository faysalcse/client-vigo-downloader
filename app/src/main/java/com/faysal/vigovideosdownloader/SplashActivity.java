package com.faysal.vigovideosdownloader;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.faysal.vigovideosdownloader.Services.AutoUrlDetect;
import com.faysal.vigovideosdownloader.Services.SharedConstant;
import com.faysal.vigovideosdownloader.helper.PrefManager;
import com.faysal.vigovideosdownloader.helper.SharedHelper;

public class SplashActivity extends AppCompatActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    PrefManager prefManager;
    Intent intent;
    private boolean download=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        intent=getIntent();

        if (intent !=null){
            download=intent.getBooleanExtra("download",false);
        }


        prefManager=new PrefManager(getApplicationContext());

        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {

                if (prefManager.isFirstTimeLaunch()){
                    Intent i = new Intent(getApplicationContext(), WelcomeActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    Log.d("lsaflsdfljsadlkfjasld", "run: "+prefManager.isFirstTimeLaunch());
                }else {
                    if (download){
                        Intent i = new Intent(getApplicationContext(), SavedActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        Log.d("lsaflsdfljsadlkfjasld", "run: "+prefManager.isFirstTimeLaunch());
                        startActivity(i);
                    }else {
                        Intent i = new Intent(getApplicationContext(), ActivityHome.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        Log.d("lsaflsdfljsadlkfjasld", "run: "+prefManager.isFirstTimeLaunch());
                        startActivity(i);
                    }
                }

                finish();

            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


    }

}
