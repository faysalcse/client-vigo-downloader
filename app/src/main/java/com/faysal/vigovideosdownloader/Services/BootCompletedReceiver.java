package com.faysal.vigovideosdownloader.Services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Faysal Ahmed Shakil on 12/10/2017.
 */

public class BootCompletedReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())){
            context.startService(new Intent(context, AutoUrlDetect.class));
        }

    }
}
