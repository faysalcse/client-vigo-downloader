package com.faysal.vigovideosdownloader.Services;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.faysal.vigovideosdownloader.DownloaderActivity;
import com.faysal.vigovideosdownloader.R;
import com.faysal.vigovideosdownloader.Utils.CommonMethods;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.util.Random;

import es.dmoral.toasty.Toasty;

public class BackgroundDownloadService extends JobIntentService {
    private static final String TAG = "download_service";
    String userUrl=null;
    static int random=new Random().nextInt();
    private int downloadId=random;

    static void enqueueWork(Context context, Intent work) {
        int rand= new Random().nextInt();
        enqueueWork(context, BackgroundDownloadService.class, random, work);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        Log.d(TAG, "onHandleWork");

        String input = intent.getStringExtra("url");

        if (input !=null && !TextUtils.isEmpty(input)){
            userUrl=input;
            scrapDataFromURL();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    @Override
    public boolean onStopCurrentWork() {
        Log.d(TAG, "onStopCurrentWork");
        return super.onStopCurrentWork();
    }


    private void scrapDataFromURL() {

        if (isVigoUrl(userUrl)==false){
           // Toast.makeText(this, getString(R.string.not_vigo_url), Toast.LENGTH_SHORT).show();
        }else {
            userUrl=getVigoVideoUrl(userUrl);
            if (isValid(userUrl)==false){
               // Toast.makeText(this, getString(R.string.enter_valid_url), Toast.LENGTH_SHORT).show();
            }else {
                if (userUrl !=null){
                    new UrlTask().execute();
                }else {
                   // Toast.makeText(this, getString(R.string.enter_valid_url), Toast.LENGTH_SHORT).show();
                }
            }
        }





    }

    /* Returns true if url is valid */
    private  boolean isValid(String url)
    {
        /* Try creating a valid URL */
        try {
            new URL(url).toURI();
            return true;
        }

        // If there was an Exception
        // while creating URL object
        catch (Exception e) {
            return false;
        }
    }

    private boolean isVigoUrl(String userUrl){
        if (userUrl.contains("www.vigovideo.net")){
            return true;
        }else {
            return false;
        }
    }


    private String getVigoVideoUrl(String userUrl){
        String ar[] = userUrl.split("\\s");


        for (String s : ar){
            // System.out.println(s);
            if (s.contains("https://www.vigovideo.net/")){
                userUrl=s;
            }
        }

        if (userUrl !=null && !TextUtils.isEmpty(userUrl)){
            return userUrl;
        }else {
            return null;
        }
    }

    class UrlTask extends AsyncTask<Void,Void,Void> {

        Document document;
        String vid_data;






        @Override
        protected Void doInBackground(Void... voids) {
            try {
                document = Jsoup.connect(userUrl).get();
                Elements formElement = document.select("div.reflow-container div#pageletReflowVideo div.video-container div.poster-page div.player-container");
                vid_data=formElement.get(0).attr("data-src");
                vid_data=extractVideoUrl(vid_data);
            } catch (IOException e) {
                e.printStackTrace();
                vid_data=null;

            }catch (RuntimeException e){
                e.printStackTrace();
                vid_data=null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void sdata) {
            if (vid_data !=null){
                downloadMedia(vid_data);
            }else {
               // Toast.makeText(getApplicationContext(), "Failed to parsing data from server !", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onPostExecute: Failed to parsing data from server !");
            }



        }
    }

    private static String extractVideoUrl(String url){
        String mainUrl=url.split("&")[0];
        String[] ids=mainUrl.split("=");
        String video_id=ids[ids.length-1];
        String e_url=null;
        if (video_id !=null){
            e_url="https://api.hypstar.com/hotsoon/item/video/_source/?video_id="+video_id;
        }
        return e_url;
    }


    public void downloadMedia(String str) {


        Log.d("sfsdfsdfdsf", "downloadMedia: "+str);
        File file = new File(CommonMethods.pathName1);
        if (!file.exists()) {
            file.mkdirs();
        }
        StringBuilder sb = new StringBuilder();
        sb.append(System.currentTimeMillis());
        sb.append(".mp4");
        final String sb2 = sb.toString();


        downloadId = PRDownloader.download(str, CommonMethods.pathName1, sb2)
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {
                       // downloadingDialog.setTitle("File downloading...");
                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {
                        PRDownloader.resume(downloadId);
                       // Toast.makeText(getApplicationContext(), "Download Paused", Toast.LENGTH_SHORT).show();
                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {
                        downloadId = random;
                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {
                        long j = (progress.currentBytes * 100) / progress.totalBytes;
                       // downloadingDialog.setProgress(Integer.parseInt(String.valueOf(j)));
                    }
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                       // downloadId = 0;
                        downloadId=random;


                        Toasty.success(getApplicationContext(), "Download Complete !", Toast.LENGTH_SHORT, true).show();
                        if (Build.VERSION.SDK_INT >= 19) {
                            Intent intent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
                            StringBuilder sb = new StringBuilder();
                            sb.append("file://");
                            sb.append(CommonMethods.pathName1);
                            intent.setData(Uri.parse(sb.toString()));
                            sendBroadcast(intent);
                            return;
                        }
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("file://");
                        sb2.append(CommonMethods.pathName1);
                        sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse(sb2.toString())));
                    }

                    @Override
                    public void onError(Error error) {
                        Log.d("sfsdfsdfdsf", "onError: "+error.getConnectionException());
                        Toasty.error(getApplicationContext(), "Download Failed !", Toast.LENGTH_SHORT, true).show();
                        StringBuilder sb = new StringBuilder();
                        sb.append(CommonMethods.pathName1);
                        sb.append(sb2);
                        File file = new File(sb.toString());
                        if (file.exists()) {
                            file.delete();
                        }
                    }
                });

        com.downloader.Status status = PRDownloader.getStatus(this.downloadId);
        PrintStream printStream = System.out;
        StringBuilder sb3 = new StringBuilder();
        sb3.append("Status of download is ::");
        sb3.append(status);
        printStream.println(sb3.toString());
    }
}
