package com.faysal.vigovideosdownloader.Services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.faysal.vigovideosdownloader.R;
import com.faysal.vigovideosdownloader.SplashActivity;

import java.util.Timer;
import java.util.TimerTask;

import static com.faysal.vigovideosdownloader.App.CHANNEL_ID;


/**
 * Created by Faysal Ahmed Shakil on 12/9/2017.
 */

public class AutoUrlDetect extends Service {

    ClipboardManager clipboard;
    String defultClip = "*FB LOADER*";
    String nullText;
    private Timer timer;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Intent notificationIntent = new Intent(this, SplashActivity.class);
        notificationIntent.putExtra("download",true);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("Copy URL to instant download")
                .setSmallIcon(R.drawable.ic_arrow_downward_black_24dp)
                .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.ic_launcher_round))
                .setContentIntent(pendingIntent)
                .build();


        clipboard = (ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);
        clipboard.setText("");
        nullText = "";

        TimerTask local1 = new TimerTask()
        {
            public void run()
            {
                if ((clipboard.hasText()) && (!clipboard.getText().toString().contentEquals(nullText))) {
                    try
                    {
                        if (!clipboard.getText().toString().contains(defultClip))
                        {

                            String url=clipboard.getText().toString();
                            nullText = clipboard.getText().toString();



                            if (pasteDataFromURL(url)){
                                Intent serviceIntent = new Intent(getApplicationContext(), BackgroundDownloadService.class);
                                serviceIntent.putExtra("url", url);

                                BackgroundDownloadService.enqueueWork(getApplicationContext(), serviceIntent);

                                Log.d("dsfgdfsgdsfgdsfgsdg", "run: "+url);


                            }


                            return;
                        }
                        nullText = clipboard.getText().toString().replace(defultClip, "");
                        clipboard.setText(nullText);
                        return;
                    }
                    catch (Exception localSQLiteException)
                    {
                        localSQLiteException.printStackTrace();
                    }
                }
            }
        };
        this.timer = new Timer();
        this.timer.schedule(local1, 0L, 1000L);


        startForeground(1, notification);

        //do heavy work on a background thread
        //stopSelf();

        return START_NOT_STICKY;
    }

    private boolean pasteDataFromURL(String obj) {
        boolean isFound = obj.indexOf("www.vigovideo.net") !=-1? true: false;
        return  isFound;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        timer.cancel();
        timer=null;
        stopSelf();
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}