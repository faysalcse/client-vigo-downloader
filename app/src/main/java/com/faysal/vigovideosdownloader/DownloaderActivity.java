package com.faysal.vigovideosdownloader;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.faysal.vigovideosdownloader.Services.AutoUrlDetect;
import com.faysal.vigovideosdownloader.Services.SharedConstant;
import com.faysal.vigovideosdownloader.Utils.CommonMethods;
import com.faysal.vigovideosdownloader.helper.SharedHelper;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class DownloaderActivity extends AppCompatActivity implements View.OnClickListener {


    @BindView(R.id.back_btn)
    ImageView back_btn;

    @BindView(R.id.share_btn)
    ImageView share_btn;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.ivClear)
    ImageView ivClear;

    @BindView(R.id.etUrl)
    EditText etUrl;

    @BindView(R.id.tvPasteLink)
    TextView tvPasteLink;

    @BindView(R.id.gotoVigo)
    LinearLayout gotoVigo;

    @BindView(R.id.ivCopyDownload)
    LinearLayout ivCopyDownload;


    String userUrl=null;
    public int downloadId;


    private AdView mAdView;
    InterstitialAd interstitial;
    private boolean interstitialLoaded=false;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloader);
        ButterKnife.bind(this);
        MobileAds.initialize(this,getString(R.string.app_id));


        initInterstital();

        back_btn.setOnClickListener(this);
        share_btn.setOnClickListener(this);
        ivClear.setOnClickListener(this);
        tvPasteLink.setOnClickListener(this);
        gotoVigo.setOnClickListener(this);
        ivCopyDownload.setOnClickListener(this);

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adRequest.isTestDevice(getApplicationContext());
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener(){

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                Toast.makeText(DownloaderActivity.this, "Failed to load ads"+i, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
            }


        });

    }

    @Override
    public void onClick(View v) {
        int id=v.getId();
        switch (id){
            case R.id.back_btn:
                finish();
                break;

             case R.id.share_btn:
                 shareApplicationLink();
                break;

             case R.id.ivClear:
                 setIvClear();
                break;

             case R.id.tvPasteLink:
                 setTvPasteLink();
                break;

             case R.id.gotoVigo:
                 setGotoVigo();
                break;

             case R.id.ivCopyDownload:
                 scrapDataFromURL();
                break;


        }
    }


    private void setGotoVigo(){
        Intent launchIntentForPackage = getPackageManager().getLaunchIntentForPackage(getString(R.string.str_vigo_video_package_name));
        if (launchIntentForPackage != null) {
            startActivity(launchIntentForPackage);
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.str_vigo_not_installed), Toast.LENGTH_SHORT).show();
        }
    }

    private void setTvPasteLink(){
        ClipboardManager cbManager = (ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData pData = cbManager.getPrimaryClip();
        ClipData.Item item = pData.getItemAt(0);
        String txtpaste = item.getText().toString();

        if (txtpaste !=null && !TextUtils.isEmpty(txtpaste)){
            etUrl.setText(txtpaste);
        }

    }

    private void setIvClear(){
        if (etUrl.getText().toString().length() >0){
            etUrl.setText("");
        }
    }


    private void shareApplicationLink(){
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Download vigo video downloader from playstore : https://play.google.com/store/apps/details?id="+ getPackageName());
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.share_with_friends)));
    }


    private void scrapDataFromURL() {
        if (etUrl.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.emptyUrlField), Toast.LENGTH_SHORT).show();
            return;
        }

        userUrl=etUrl.getText().toString().trim();

        if (isVigoUrl(userUrl)==false){
            Toast.makeText(this, getString(R.string.not_vigo_url), Toast.LENGTH_SHORT).show();
        }else {
            userUrl=getVigoVideoUrl(userUrl);
            if (isValid(userUrl)==false){
                Toast.makeText(this, getString(R.string.enter_valid_url), Toast.LENGTH_SHORT).show();
            }else {
                if (userUrl !=null){
                    new UrlTask().execute();
                }else {
                    Toast.makeText(this, getString(R.string.enter_valid_url), Toast.LENGTH_SHORT).show();
                }
            }
        }





    }

    /* Returns true if url is valid */
    private  boolean isValid(String url)
    {
        /* Try creating a valid URL */
        try {
            new URL(url).toURI();
            return true;
        }

        // If there was an Exception
        // while creating URL object
        catch (Exception e) {
            return false;
        }
    }

    private boolean isVigoUrl(String userUrl){
        if (userUrl.contains("www.vigovideo.net")){
            return true;
        }else {
            return false;
        }
    }


    private String getVigoVideoUrl(String userUrl){
        String ar[] = userUrl.split("\\s");


        for (String s : ar){
            // System.out.println(s);
            if (s.contains("https://www.vigovideo.net/")){
                userUrl=s;
            }
        }

        if (userUrl !=null && !TextUtils.isEmpty(userUrl)){
            return userUrl;
        }else {
            return null;
        }
    }

    public class UrlTask extends AsyncTask<Void,Void,Void> {

        Document document;
        ProgressDialog dialog;
        String vid_data;




        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog=new ProgressDialog(DownloaderActivity.this);
            dialog.setMessage("Please wait....");
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                document = Jsoup.connect(userUrl).get();
                Elements formElement = document.select("div.reflow-container div#pageletReflowVideo div.video-container div.poster-page div.player-container");
                vid_data=formElement.get(0).attr("data-src");
                vid_data=extractVideoUrl(vid_data);
            } catch (IOException e) {
                e.printStackTrace();
                vid_data=null;

            }catch (RuntimeException e){
                e.printStackTrace();
                vid_data=null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void sdata) {
            if (vid_data !=null){
                downloadMedia(vid_data);
            }else {
                Toast.makeText(DownloaderActivity.this, "Failed to parsing data from server !", Toast.LENGTH_SHORT).show();
            }

            dialog.dismiss();


        }
    }

    private static String extractVideoUrl(String url){
        String mainUrl=url.split("&")[0];
        String[] ids=mainUrl.split("=");
        String video_id=ids[ids.length-1];
        String e_url=null;
        if (video_id !=null){
            e_url="https://api.hypstar.com/hotsoon/item/video/_source/?video_id="+video_id;
        }
        return e_url;
    }


    public void downloadMedia(String str) {

        initInterstital();

        Log.d("sfsdfsdfdsf", "downloadMedia: "+str);
        showDownloadingDialog();
        File file = new File(CommonMethods.pathName1);
        if (!file.exists()) {
            file.mkdirs();
        }
        StringBuilder sb = new StringBuilder();
        sb.append(System.currentTimeMillis());
        sb.append(".mp4");
        final String sb2 = sb.toString();


       downloadId = PRDownloader.download(str, CommonMethods.pathName1, sb2)
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {
                        downloadingDialog.setTitle("File downloading...");
                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {
                        PRDownloader.resume(downloadId);
                        Toast.makeText(getApplicationContext(), "Download Paused", Toast.LENGTH_SHORT).show();
                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {
                        downloadId = 0;
                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {
                        long j = (progress.currentBytes * 100) / progress.totalBytes;
                        downloadingDialog.setProgress(Integer.parseInt(String.valueOf(j)));
                    }
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        downloadId = 0;

                        if (interstitialLoaded){

                            displayInterstitial();
                        }

                        Toasty.success(DownloaderActivity.this, "Download Complete !", Toast.LENGTH_SHORT, true).show();
                        dismissDownloadingDialog();
                        if (Build.VERSION.SDK_INT >= 19) {
                            Intent intent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
                            StringBuilder sb = new StringBuilder();
                            sb.append("file://");
                            sb.append(CommonMethods.pathName1);
                            intent.setData(Uri.parse(sb.toString()));
                            sendBroadcast(intent);
                            return;
                        }
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("file://");
                        sb2.append(CommonMethods.pathName1);
                        sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse(sb2.toString())));
                    }

                    @Override
                    public void onError(Error error) {
                        dismissDownloadingDialog();
                        Log.d("sfsdfsdfdsf", "onError: "+error.getConnectionException());
                        Toasty.error(DownloaderActivity.this, "Download Failed !", Toast.LENGTH_SHORT, true).show();
                        StringBuilder sb = new StringBuilder();
                        sb.append(CommonMethods.pathName1);
                        sb.append(sb2);
                        File file = new File(sb.toString());
                        if (file.exists()) {
                            file.delete();
                        }
                    }
                });

        com.downloader.Status status = PRDownloader.getStatus(this.downloadId);
        PrintStream printStream = System.out;
        StringBuilder sb3 = new StringBuilder();
        sb3.append("Status of download is ::");
        sb3.append(status);
        printStream.println(sb3.toString());
    }


    ProgressDialog downloadingDialog;

    private void showDownloadingDialog() {
        if (downloadingDialog == null) {
            downloadingDialog = new ProgressDialog(this);
            downloadingDialog.setMessage("Downloading file..");
            downloadingDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            downloadingDialog.setCancelable(false);
            downloadingDialog.show();
        }
        downloadingDialog.show();
    }
    /* access modifiers changed from: private */
    public void dismissDownloadingDialog() {
        Dialog dialog = this.downloadingDialog;
        if (dialog != null && dialog.isShowing()) {
            this.downloadingDialog.dismiss();
            etUrl.setText("");
        }
    }



    public void onDestroy() {

        if (downloadingDialog !=null){
            downloadingDialog.dismiss();
        }
        dismissDownloadingDialog();
        super.onDestroy();
    }





    private void initInterstital(){

        AdRequest adIRequest = new AdRequest.Builder().build();

        // Prepare the Interstitial Ad Activity
        interstitial = new InterstitialAd(DownloaderActivity.this);

        // Insert the Ad Unit ID
        interstitial.setAdUnitId(getString(R.string.interstitial_ads));

        // Interstitial Ad load Request
        interstitial.loadAd(adIRequest);

        // Prepare an Interstitial Ad Listener
        interstitial.setAdListener(new AdListener()
        {
            public void onAdLoaded()
            {
                // Call displayInterstitial() function when the Ad loads
                interstitialLoaded=true;
            }
        });
    }

    public void displayInterstitial()
    {
        // If Interstitial Ads are loaded then show else show nothing.
        if (interstitial.isLoaded()) {
            interstitial.show();
        }
    }


}
