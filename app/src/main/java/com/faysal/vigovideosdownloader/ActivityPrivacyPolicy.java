package com.faysal.vigovideosdownloader;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.webkit.WebView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.faysal.vigovideosdownloader.Services.AutoUrlDetect;
import com.faysal.vigovideosdownloader.Services.SharedConstant;
import com.faysal.vigovideosdownloader.helper.SharedHelper;

public class ActivityPrivacyPolicy extends AppCompatActivity {


    WebView wv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);

        wv = (WebView) findViewById(R.id.webview);
        wv.loadUrl("file:///android_asset/privacy.html");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
