package com.faysal.vigovideosdownloader;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.faysal.vigovideosdownloader.Adapters.MediaAdapter;
import com.faysal.vigovideosdownloader.Services.AutoUrlDetect;
import com.faysal.vigovideosdownloader.Services.SharedConstant;
import com.faysal.vigovideosdownloader.Utils.CommonMethods;
import com.faysal.vigovideosdownloader.helper.SharedHelper;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.universalvideoview.UniversalMediaController;
import com.universalvideoview.UniversalVideoView;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class PlayerActivity extends AppCompatActivity {

    public static ArrayList<String> results = new ArrayList<>();
    private static final String TAG = PlayerActivity.class.getSimpleName() ;
    int positions=0;
    UniversalVideoView mVideoView;
    UniversalMediaController mMediaController;

    @BindView(R.id.ivPrevious)
    ImageView ivPrevious;

    @BindView(R.id.ivNext)
    ImageView ivNext;

    @BindView(R.id.ivVideoDelete)
    ImageView ivVideoDelete;

    @BindView(R.id.ivVideoShare)
    ImageView ivVideoShare;

    @BindView(R.id.ivInfo)
    ImageView ivInfo;

    private static final DecimalFormat format = new DecimalFormat("#.##");
    private static final long MiB = 1024 * 1024;
    private static final long KiB = 1024;

    private AdView mAdView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        ButterKnife.bind(this);
        fetchingMediaFromDirectory();

        MobileAds.initialize(this,getString(R.string.app_id));

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adRequest.isTestDevice(getApplicationContext());
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener(){

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                Toast.makeText(PlayerActivity.this, "Failed to load ads"+i, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
            }


        });




        mVideoView = (UniversalVideoView) findViewById(R.id.videoView);
        mMediaController = (UniversalMediaController) findViewById(R.id.media_controller);

        Intent intent=getIntent();
        if (intent !=null){
            positions=intent.getIntExtra("vid_pos",0);
            playVideo(positions);
        }

        ivPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                positions=positions-1;
                if (positions == -1){
                    positions=results.size()-1;
                    Log.d(TAG, "onClick: if "+positions);
                    playVideo(positions);
                }else {

                    Log.d(TAG, "onClick: else "+positions);
                    playVideo(positions);
                }
            }
        });

        ivNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                positions=positions+1;
                if (positions == results.size()){
                    positions=0;
                    playVideo(positions);
                }else {
                    playVideo(positions);
                }
            }
        });

        ivVideoDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String path=results.get(positions);

                Log.d("ISDSDFSDF", "onClick: "+path);

                File fdelete = new File(path);
                if (fdelete.exists()) {
                    if (fdelete.delete()) {
                        System.out.println("file Deleted :" + path);
                        Toasty.success(getApplicationContext(),"This video file has been deleted", Toasty.LENGTH_LONG).show();
                        results.remove(positions);
                        finish();
                    } else {
                        Toasty.error(getApplicationContext(),"Filed to delete this video", Toasty.LENGTH_LONG).show();
                    }
                }

            }
        });

        ivVideoShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.shareMediaThroughIntent("video/*", (String) results.get(positions), 1, PlayerActivity.this);
            }
        });

        ivInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File file = new File(results.get(positions));
                Date lastModDate = new Date(file.lastModified());

                videoInfoDialoge(
                       file.getName(),
                        file.getPath(),
                        getFileSize(file),
                        lastModDate.toString()
                );
            }
        });


    }

    public String getFileSize(File file) {

        if (!file.isFile()) {
            throw new IllegalArgumentException("Expected a file");
        }
        final double length = file.length();

        if (length > MiB) {
            return format.format(length / MiB) + " MiB";
        }
        if (length > KiB) {
            return format.format(length / KiB) + " KiB";
        }
        return format.format(length) + " B";
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mVideoView.stopPlayback();
        mVideoView=null;
        finish();
    }

    public void playVideo(int pos){


        mVideoView.setMediaController(mMediaController);
        mVideoView.setVideoViewCallback(new UniversalVideoView.VideoViewCallback() {
            private int cachedHeight;

            @Override
            public void onScaleChange(boolean isFullscreen) {

            }

            @Override
            public void onPause(MediaPlayer mediaPlayer) { // Video pause
                Log.d(TAG, "onPause UniversalVideoView callback");
            }

            @Override
            public void onStart(MediaPlayer mediaPlayer) { // Video start/resume to play
                Log.d(TAG, "onStart UniversalVideoView callback");
            }

            @Override
            public void onBufferingStart(MediaPlayer mediaPlayer) {// steam start loading
                Log.d(TAG, "onBufferingStart UniversalVideoView callback");
            }

            @Override
            public void onBufferingEnd(MediaPlayer mediaPlayer) {// steam end loading

            }



        });

        mVideoView.setVideoPath(results.get(pos));
        mVideoView.start();
        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

                if (mp !=null){
                    positions=pos+1;

                    if (positions == results.size()){
                        positions=0;
                        playVideo(positions);
                    }else {
                        playVideo(positions);
                    }
                }

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        playVideo(positions);
    }

    public static void fetchingMediaFromDirectory() {
        results.clear();
        File[] listFiles = new File(CommonMethods.pathName1).listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                if (file.isFile()) {
                    ArrayList<String> arrayList = results;
                    StringBuilder sb = new StringBuilder();
                    sb.append(CommonMethods.pathName1);
                    sb.append(file.getName());
                    arrayList.add(sb.toString());
                }
            }
        }


        Collections.sort(results);
        Collections.reverse(results);

    }


    public void videoInfoDialoge(String name,String location,String size,String date){
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.video_info_dialog, viewGroup, false);


        TextView tvFileName=dialogView.findViewById(R.id.tvFileName);
        TextView tvLocation=dialogView.findViewById(R.id.tvLocation);
        TextView tvSize=dialogView.findViewById(R.id.tvSize);
        TextView tvDate=dialogView.findViewById(R.id.tvDate);

        if (name !=null && !TextUtils.isEmpty(name)){
            tvFileName.setText(name);
        }

        if (location !=null && !TextUtils.isEmpty(location)){
            tvLocation.setText(location);
        }

        if (size !=null && !TextUtils.isEmpty(size)){
            tvSize.setText(size);
        }

        if (date !=null && !TextUtils.isEmpty(date)){
            tvDate.setText(date);
        }

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);




        builder.setNegativeButton(R.string.cancle, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }



}
