package com.faysal.vigovideosdownloader;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.faysal.vigovideosdownloader.Services.AutoUrlDetect;
import com.faysal.vigovideosdownloader.Services.SharedConstant;
import com.faysal.vigovideosdownloader.Utils.CommonMethods;
import com.faysal.vigovideosdownloader.helper.SharedHelper;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityHome extends AppCompatActivity {


    private static final int PERMISSION_REQUEST_CODE = 4345 ;
    @BindView(R.id.ivCopyUrlAndDownloadVideo)
    LinearLayout ivCopyUrlAndDownloadVideo;

    @BindView(R.id.ivDownload)
    LinearLayout ivDownload;

    @BindView(R.id.ivSetting)
    LinearLayout ivSetting;

    @BindView(R.id.ivVigoApp)
    LinearLayout ivVigoApp;

    @BindView(R.id.llhowitWork)
    CardView llhowitWork;


    @BindView(R.id.rate_btn)
    ImageView rate_btn;

    @BindView(R.id.share_btn)
    ImageView share_btn;

    private AdView mAdView;

    InterstitialAd interstitial;
    private boolean interstitialLoaded=false;

    SwitchCompat switchCompact;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        MobileAds.initialize(this,getString(R.string.app_id));

        switchCompact=findViewById(R.id.switchCompact);
        switchCompact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (switchCompact.isChecked()){
                    Toast.makeText(ActivityHome.this, "Auto Download Service is enable", Toast.LENGTH_SHORT).show();
                    SharedHelper.putKey(getApplicationContext(), SharedConstant.AUTO_DOWNLOAD,"true");
                    ContextCompat.startForegroundService(getApplicationContext(),new Intent(getApplicationContext(), AutoUrlDetect.class));
                }else {
                    Toast.makeText(ActivityHome.this, "Auto Download Service is disable", Toast.LENGTH_SHORT).show();
                    SharedHelper.putKey(getApplicationContext(), SharedConstant.AUTO_DOWNLOAD,"false");
                    stopService(new Intent(ActivityHome.this, AutoUrlDetect.class));
                }
            }
        });



        if (SharedHelper.getKey(getApplicationContext(), SharedConstant.AUTO_DOWNLOAD).equals("true")){
            switchCompact.setChecked(true);
            ContextCompat.startForegroundService(getApplicationContext(),new Intent(getApplicationContext(), AutoUrlDetect.class));
        }else {
            switchCompact.setChecked(false);
            stopService(new Intent(getApplicationContext(), AutoUrlDetect.class));
        }

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adRequest.isTestDevice(getApplicationContext());
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener(){

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                Toast.makeText(ActivityHome.this, "Failed to load ads"+i, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
            }


        });




        ivCopyUrlAndDownloadVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(), "android.permission.READ_EXTERNAL_STORAGE") != 0) {
                    Toast.makeText(ActivityHome.this, getString(R.string.permission_missing), Toast.LENGTH_SHORT).show();
                }else {
                    startActivity(new Intent(getApplicationContext(),DownloaderActivity.class));
                }

            }
        });

        ivDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),SavedActivity.class));
            }
        });

        ivSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),SetttingsActivity.class));
            }
        });

        ivVigoApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setGotoVigo();
            }
        });

        llhowitWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(ActivityHome.this, "This app is underdevelopment", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),WelcomeActivity.class));
            }
        });

        share_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareApplicationLink();
            }
        });

        rate_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });




        if (ContextCompat.checkSelfPermission(this, "android.permission.READ_EXTERNAL_STORAGE") != 0) {
            askPermission_gallery();
        } else {
            CommonMethods.createDirectory();
            System.out.println("Code reader cannot come here");
        }
    }


    private void shareApplicationLink(){
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Download vigo video downloader from playstore : https://play.google.com/store/apps/details?id="+ getPackageName());
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.share_with_friends)));
    }

    /* access modifiers changed from: 0000 */
    public void askPermission_gallery() {
        if (Build.VERSION.SDK_INT < 23) {
            CommonMethods.createDirectory();
        } else if (ContextCompat.checkSelfPermission(this, "android.permission.READ_EXTERNAL_STORAGE") == 0 || ContextCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE") == 0) {
            CommonMethods.createDirectory();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"}, PERMISSION_REQUEST_CODE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    CommonMethods.createDirectory();
            }else {
                Toast.makeText(this, getString(R.string.permission_missing), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initInterstital(){

        AdRequest adIRequest = new AdRequest.Builder().build();

        // Prepare the Interstitial Ad Activity
        interstitial = new InterstitialAd(ActivityHome.this);

        // Insert the Ad Unit ID
        interstitial.setAdUnitId(getString(R.string.interstitial_ads));

        // Interstitial Ad load Request
        interstitial.loadAd(adIRequest);

        // Prepare an Interstitial Ad Listener
        interstitial.setAdListener(new AdListener()
        {
            @Override
            public void onAdLoaded() {
               interstitialLoaded=true;

            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                Toast.makeText(ActivityHome.this, "Failed to load ads", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void displayInterstitial()
    {
        // If Interstitial Ads are loaded then show else show nothing.
        if (interstitial.isLoaded()) {
            interstitial.show();
        }
    }


    private void setGotoVigo(){
        Intent launchIntentForPackage = getPackageManager().getLaunchIntentForPackage(getString(R.string.str_vigo_video_package_name));
        if (launchIntentForPackage != null) {
            startActivity(launchIntentForPackage);
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.str_vigo_not_installed), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
