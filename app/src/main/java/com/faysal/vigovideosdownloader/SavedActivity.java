package com.faysal.vigovideosdownloader;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.faysal.vigovideosdownloader.Adapters.MediaAdapter;
import com.faysal.vigovideosdownloader.Services.AutoUrlDetect;
import com.faysal.vigovideosdownloader.Services.SharedConstant;
import com.faysal.vigovideosdownloader.Utils.CommonMethods;
import com.faysal.vigovideosdownloader.helper.SharedHelper;
import com.google.android.gms.ads.AdView;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SavedActivity extends AppCompatActivity implements View.OnClickListener {

    private static Activity activity;
    public static ImageView delete_btn;
    public static MediaAdapter mediaAdapter;
    public static LinearLayout noMedia;
    public static GridView photos_grid;
    public static ArrayList<String> results = new ArrayList<>();
    /* access modifiers changed from: private */
    public static boolean setMultipleSelection = false;
    private TextView no_media;
    SharedPreferences sharedPreferences;
    private AdView adView;

    @BindView(R.id.back_btn)
    ImageView back_btn;

    @BindView(R.id.share_btn)
    ImageView share_btn;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.contentLayout)
    LinearLayout contentLayout;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved);
        ButterKnife.bind(this);

        activity = SavedActivity.this;
        this.sharedPreferences = getSharedPreferences("musically", 0);
        CommonMethods.is_a_premium_member = Boolean.valueOf(this.sharedPreferences.getBoolean("isapremiummember", false));
        setMultipleSelection = false;
        this.no_media = (TextView)findViewById(R.id.no_media);
        noMedia = (LinearLayout)findViewById(R.id.noMedia);
        photos_grid = (GridView)findViewById(R.id.photos_grid);
      /*  mediaAdapter = new MediaAdapter(activity, results);
        photos_grid.setAdapter(mediaAdapter);*/
        //  this.no_media.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/billabong.ttf"));



        back_btn.setOnClickListener(this);
        share_btn.setOnClickListener(this);


    }

    @Override
    protected void onResume() {
        super.onResume();
        mediaAdapter = new MediaAdapter(activity, results);
        photos_grid.setAdapter(mediaAdapter);
        fetchingMediaFromDirectory();
    }

    public  void fetchingMediaFromDirectory() {
        results.clear();
        File[] listFiles = new File(CommonMethods.pathName1).listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                if (file.isFile()) {
                    ArrayList<String> arrayList = results;
                    StringBuilder sb = new StringBuilder();
                    sb.append(CommonMethods.pathName1);
                    sb.append(file.getName());
                    arrayList.add(sb.toString());
                }
            }
        }
        if (results.size() == 0) {
            noMedia.setVisibility(View.VISIBLE);
            contentLayout.setVisibility(View.GONE);
            return;
        }
        Collections.sort(results);
        Collections.reverse(results);
        noMedia.setVisibility(View.GONE);
        mediaAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.back_btn:
                finish();
                break;

            case R.id.share_btn:
                shareApplicationLink();
                break;

        }
    }

    private void shareApplicationLink(){
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Download vigo video downloader from playstore : https://play.google.com/store/apps/details?id="+ getPackageName());
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.share_with_friends)));
    }
}
